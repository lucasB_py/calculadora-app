import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  subDisplayText = ''; // Pantalla secundaria de la calculadora
  mainDisplayText = ''; // Pantalla principal de la calculadora
  num1: number; // Primer numero
  num2: number; // Segundo numero
  operador = ''; // Operador
  calculationString = ''; // Calculo de operador y numeros
  answered = false;
  operatorSet = false;

  pressKey(key: string) {
    if (key === '/' || key === 'x' || key === '-' || key === '+') {
      const lastKey = this.mainDisplayText[this.mainDisplayText.length - 1];
      
      if (lastKey === '/' || lastKey === 'x' || lastKey === '-' || lastKey === '+') {
        this.operatorSet = true;
      }
      if ((this.operatorSet) || (this.mainDisplayText === '')) {
        return;
      }
      this.num1 = parseFloat(this.mainDisplayText);
      this.operador = key;
      this.operatorSet = true;
    }
    if (this.mainDisplayText.length === 10) {
      return;
    }
    this.mainDisplayText += key;
  }

  allClear() {
    this.mainDisplayText = '';
    this.subDisplayText = '';
    this.operatorSet = false;
  }

  getAnswer() {
    this.calculationString = this.mainDisplayText;
    this.num2 = parseFloat(this.mainDisplayText.split(this.operador)[1]);
    
    if (this.operador === '/') {
      this.subDisplayText = this.mainDisplayText;
      this.mainDisplayText = (this.num1 / this.num2).toString();
      this.subDisplayText = this.calculationString;

      if (this.mainDisplayText.length > 9) {
        this.mainDisplayText = this.mainDisplayText.substr(0, 9);
      }

    } else if (this.operador === 'x') {
      this.subDisplayText = this.mainDisplayText;
      this.mainDisplayText = (this.num1 * this.num2).toString();
      this.subDisplayText = this.calculationString;

      if (this.mainDisplayText.length > 9) {
        this.mainDisplayText = 'ERROR';
        this.subDisplayText = 'Rango excedido';
      }
    } else if (this.operador === '-') {
      this.subDisplayText = this.mainDisplayText;
      this.mainDisplayText = (this.num1 - this.num2).toString();
      this.subDisplayText = this.calculationString;

    } else if (this.operador === '+') {
      this.subDisplayText = this.mainDisplayText;
      this.mainDisplayText = (this.num1 + this.num2).toString();
      this.subDisplayText = this.calculationString;

      if (this.mainDisplayText.length > 9) {
        this.mainDisplayText = 'ERROR';
        this.subDisplayText = 'Rango excedido';
      }
    } else {
      this.subDisplayText = 'ERROR: Operación Inválida';
    }
    this.answered = true;
  }
  

  constructor() { }

  ngOnInit(): void {
  }

}
