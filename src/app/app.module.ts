import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './pages/calculator/calculator.component';
import { HeaderComponent } from './pages/header/header.component';
import { HomeComponent } from './pages/home.component';

const rutas: Routes = [
  { path: '', component: HomeComponent },
  { path: 'calculadora', component: CalculatorComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    RouterModule.forRoot(rutas),
    BrowserModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
